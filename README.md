# Audio Stream Example

Streams audio files

Based on:
https://technicalsand.com/streaming-data-spring-boot-restful-web-service/

## build & run

To build the project execute

```bash
mvn clean install
```

To start the server run

```bash
java -jar target/audiostreamexample-0.0.1-SNAPSHOT.jar
```

| URL                                     | Description                         |
|:----------------------------------------|:------------------------------------|
| http://localhost:8080/                  | loads index.html with \<audio\> tag |
| http://127.0.0.1:8080/audios/audio1.mp3 | streams audio file audio1.mp3       |