package at.ac.fhsalzburg.sst.audiostream.audiostreamexample;

import org.springframework.content.fs.config.EnableFilesystemStores;
import org.springframework.content.fs.io.FileSystemResourceLoader;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;

@Configuration
@EnableFilesystemStores
public class ApplicationConfig {

    @Bean
    File filesystemRoot() throws IOException {
        return Files.createTempDirectory("").toFile();
    }

    @Bean
    public FileSystemResourceLoader fileSystemResourceLoader() throws IOException {
        return new FileSystemResourceLoader(filesystemRoot().getAbsolutePath());
    }
}
