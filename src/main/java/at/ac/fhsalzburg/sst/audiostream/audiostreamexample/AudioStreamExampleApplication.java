package at.ac.fhsalzburg.sst.audiostream.audiostreamexample;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AudioStreamExampleApplication {

    public static void main(String[] args) {
        SpringApplication.run(AudioStreamExampleApplication.class, args);
    }

}
